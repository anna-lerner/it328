//Van Anderson
//IT 328 Program1
//converts a 3SAT problem to a k Clique problem, which is then solved by calling 
//a method from findClique which then reduces to a vertex cover finding the answer.
import java.util.Random;
import java.io.FileNotFoundException;
import java.io.File;
import java.util.Scanner;
import java.util.ArrayList;

public class find3SAT
{
    static int cnfNum = 0;
    static Random rand = new Random();

    public static int[][] threeSatToClique(ArrayList<Integer> cnf)
    {
        cnfNum++;
        int maxVar = 0;
        int numEdges = 0;
        int sameClauseNum1;
        int sameClauseNum2;
        int[] assignemntArray = new int[cnf.size() + 1];
        int[][] cliqueArray = new int[cnf.size()][cnf.size()];
        // k is the necessary clique size to be found
        int k = cnf.size() / 3;
        for (int i = 0; i < cnf.size(); i++)
        {
            if (cnf.get(i) > maxVar)
            {
                maxVar = cnf.get(i);
            }
            //correctly ensures that no numbers in the same clause have an edge between them
            if (i % 3 == 0)
            {
                sameClauseNum1 = i + 1;
                sameClauseNum2 = i + 2;
            }
            else if (i % 3 == 1)
            {
                sameClauseNum1 = i - 1;
                sameClauseNum2 = i + 1;
            }
            else
            {
                sameClauseNum1 = i - 2;
                sameClauseNum2 = i - 1;
            }
            //goes through the input(in the form of the cnf arraylist and converts it to a 2d array representation of the graph, 
            //number that are complements or are in the same clause do not get edges between them)
            for (int j = 0; j < cnf.size(); j++)
            {
                if (cnf.get(j) == cnf.get(i) * -1 || j == sameClauseNum1 || j == sameClauseNum2)
                {
                    cliqueArray[i][j] = 0;
                    cliqueArray[j][i] = 0;
                }
                else
                {
                    cliqueArray[i][j] = 1;
                    numEdges++;
                    cliqueArray[j][i] = 1;
                }
            }
        }
        // if the size of the clique is greater or equal to k it exists, a clique
        // is a collection of vertecies that are all adjacent
        
        //times the completion of doing the reductions and receiving an answer
        long startTime = System.currentTimeMillis();
        System.out.println("3CNF No." + cnfNum + ": [n=" + maxVar + " c=" + k + "] ==> Clique: [V=" + cnf.size() + " E="
                + (numEdges - cnf.size()) / 2 + " k=" + k + "]");
        assignemntArray = findClique.findGraphClique(cliqueArray,k);
        //assignemntArray[0]=0;
        long totalTime = System.currentTimeMillis() - startTime;
        int[] varAssignments = new int[maxVar];
        //if a clique of size k does not exist, a random assignment is tried, at least one clause will be all false
        if (assignemntArray[0] == 0)
        {

            System.out.print("In " + totalTime + " ms, find no solution! Random assignment [");
            for (int i = 0; i < varAssignments.length; i++)
            {
                if (rand.nextInt(2) == 0)
                {
                    varAssignments[i] = -1;
                }
                else
                {
                    varAssignments[i] = 1;
                }

                System.out.print(i + 1 + ":");
                if (varAssignments[i] == 1)
                {
                    System.out.print("T ");
                }
                else
                {
                    System.out.print("F ");
                }

            }
            System.out.println("\b]");
        }
        else
        {
            for (int i = 1; i < assignemntArray.length; i++)
            {
                if (cnf.get(assignemntArray[i]) * -1 > 0)
                {
                    varAssignments[(cnf.get(assignemntArray[i]) * -1) - 1] = -1;
                }
                else
                {
                    varAssignments[cnf.get(assignemntArray[i]) - 1] = 1;
                }
            }
            System.out.print("In " + totalTime + " ms, find solution [");
            //prints the solution given the vertices that are in the k clique
            for (int i = 0; i < varAssignments.length; i++)
            {

                System.out.print(i + 1 + ":");
                if (varAssignments[i] == 1)
                {
                    System.out.print("T ");
                }
                else if (varAssignments[i] == -1)
                {
                    System.out.print("F ");
                }
                else
                {
                    System.out.print("X ");
                }

            }
            System.out.println("\b]");

        }
        System.out.print("(");
        for (int i = 0; i < cnf.size() - 1; i++)
        {
            if (cnf.get(i) < 0)
            {
                System.out.print(cnf.get(i) + "|");

            }
            else
            {
                System.out.print(" " + cnf.get(i) + "|");
            }
            if ((i + 1) % 3 == 0)
            {
                System.out.print(")/\\(");
            }

        }
        System.out.println(cnf.get(cnf.size() - 1) + ") ==>");
        System.out.print("(");
        for (int i = 0; i < cnf.size() - 1; i++)
        {
            if (cnf.get(i) < 0)
            {
                if (varAssignments[(cnf.get(i) * -1) - 1] == 1)
                {
                    System.out.print(" F|");
                }
                else if (varAssignments[(cnf.get(i) * -1) - 1] == -1)
                {
                    System.out.print(" T|");
                }
                else
                {
                    System.out.print(" X|");
                }
            }
            else
            {
                if (varAssignments[cnf.get(i) - 1] == 1)
                {
                    System.out.print(" T|");
                }
                else if (varAssignments[cnf.get(i) - 1] == -1)
                {
                    System.out.print(" F|");
                }
                else
                {
                    System.out.print(" X|");
                }
            }
            if ((i + 1) % 3 == 0)
            {
                System.out.print(")/\\(");
            }
        }
        if (cnf.get(cnf.size() - 1) < 0)
        {
            if (varAssignments[(cnf.get(cnf.size() - 1) * -1) - 1] == 1)
            {
                System.out.print(" F");
            }
            else if (varAssignments[(cnf.get(cnf.size() - 1) * -1) - 1] == -1)
            {
                System.out.print(" T");
            }
            else
            {
                System.out.print(" X");
            }
        }
        else
        {
            if (varAssignments[cnf.get(cnf.size() - 1) - 1] == 1)
            {
                System.out.print(" T");
            }
            else if (varAssignments[cnf.get(cnf.size() - 1) - 1] == -1)
            {
                System.out.print(" F");
            }
            else
            {
                System.out.print(" X");
            }
        }
        System.out.println(")\n");
        return cliqueArray;
    }

    public static void main(String[] args)
    {
        try
        {
            File myFile = new File(args[0]);
            //File myFile = new File("cnfs2024.txt");
            Scanner fileReader = new Scanner(myFile);
            ArrayList<Integer> cnfList = new ArrayList<Integer>();
            System.out.println("* Solve 3CNF in "+ args[0]+ ": (reduced to k-clique) *\n" + 
                                "X means can be either T or F\n");
            while (fileReader.hasNextLine())
            {
                String curLine = fileReader.nextLine();
                String[] stringNums = curLine.split(" ");
                for (int i = 0; i < stringNums.length; i++)
                {
                    // System.out.print(stringNums[i] + " ");
                    cnfList.add(Integer.parseInt(stringNums[i]));
                }
                threeSatToClique(cnfList);

                cnfList.clear();
            }
            fileReader.close();
        }
        catch (FileNotFoundException e)
        {
            System.out.println("File not found");

        }

    }
}
